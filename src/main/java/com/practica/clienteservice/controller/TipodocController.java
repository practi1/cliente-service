package com.practica.clienteservice.controller;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.practica.clienteservice.dto.entrada.ITipodocDto;
import com.practica.clienteservice.dto.salida.OTipodocDto;
import com.practica.clienteservice.service.TipodocService;
import com.practica.commonsmodule.error.FaltaArgumento;
import com.practica.commonsmodule.utilis.Convert;

@RestController
@RequestMapping("/tipodocs")
@RolesAllowed("admin")
public class TipodocController {

    @Autowired
    TipodocService tipodocService;
    
    @GetMapping("/all")
    public ResponseEntity<List<OTipodocDto>> list() {
    	return new ResponseEntity<>(tipodocService.findAll(), HttpStatus.OK); 
    }
    
    @GetMapping
    public ResponseEntity<Page<OTipodocDto>> getAllPages(
                        @RequestParam(defaultValue = "0") Integer pageNo, 
                        @RequestParam(defaultValue = "10") Integer pageSize,
                        @RequestParam(defaultValue = "id") String sortBy) {
    	return new ResponseEntity<>(tipodocService.getAllPages(pageNo, pageSize, sortBy), HttpStatus.OK); 
    }

    @GetMapping("/{id}")
    public ResponseEntity<OTipodocDto> get(@PathVariable int id) {
    	return new ResponseEntity<>(tipodocService.findOne(id),HttpStatus.OK) ;
    }

    @PostMapping("")
    public ResponseEntity<OTipodocDto> add(@Valid @RequestBody ITipodocDto iTipodoc, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(tipodocService.save(iTipodoc), HttpStatus.CREATED);
    }

    @PutMapping("")
    public ResponseEntity<OTipodocDto> update(@Valid @RequestBody ITipodocDto iTipodoc, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(tipodocService.update(iTipodoc), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<OTipodocDto> delete(@PathVariable int id) {
    	return new ResponseEntity<>(tipodocService.deleteOne(id), HttpStatus.OK);
    }

}
