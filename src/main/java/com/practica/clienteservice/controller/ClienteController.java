package com.practica.clienteservice.controller;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.practica.clienteservice.dto.entrada.IClienteDto;
import com.practica.clienteservice.dto.entrada.IClienteImagenDto;
import com.practica.clienteservice.dto.salida.OClienteDto;
import com.practica.clienteservice.dto.salida.OClienteImagenDto;
import com.practica.clienteservice.service.CiudadService;
import com.practica.clienteservice.service.ClienteService;
import com.practica.clienteservice.service.TipodocService;
import com.practica.commonsmodule.error.FaltaArgumento;
import com.practica.commonsmodule.utilis.Convert;

@RestController
@RequestMapping("/clientes")
@RolesAllowed("admin")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @Autowired
    TipodocService tipodocService;

    @Autowired
    CiudadService ciudadService;
    
    @GetMapping("/all")
    public ResponseEntity<List<OClienteDto>> list() {
    	return new ResponseEntity<>(clienteService.findAll(), HttpStatus.OK);
    }
    
    @GetMapping
    public ResponseEntity<Page<OClienteDto>> getAllPages(
                        @RequestParam(defaultValue = "0") Integer pageNo, 
                        @RequestParam(defaultValue = "10") Integer pageSize,
                        @RequestParam(defaultValue = "id") String sortBy) {
    	return new ResponseEntity<>(clienteService.getAllPages(pageNo, pageSize, sortBy), HttpStatus.OK); 
    }

    @GetMapping("/{id}")
    public ResponseEntity<OClienteDto> get(@PathVariable int id) {
    	return new ResponseEntity<>(clienteService.findOne(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<OClienteDto> add(@Valid @RequestBody IClienteDto iCliente, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(clienteService.save(iCliente), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<OClienteDto> update(@Valid @RequestBody IClienteDto iCliente, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(clienteService.update(iCliente), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<OClienteDto> delete(@PathVariable int id) {
    	return new ResponseEntity<>(clienteService.deleteOne(id), HttpStatus.OK);
    }
    
    //endpoint requeridos
    
    @GetMapping("/imagen")
    public ResponseEntity<Page<OClienteImagenDto>> getAllPagesImagen(
            @RequestParam(defaultValue = "0") Integer pageNo, 
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy) {
    	return new ResponseEntity<>(clienteService.getAllPagesImagen(pageNo, pageSize, sortBy), HttpStatus.OK); 
	}

    @GetMapping("/imagen/{id}")
    public ResponseEntity<OClienteImagenDto> getImagen(@PathVariable int id) {
    	return new ResponseEntity<>(clienteService.findOneImagen(id), HttpStatus.OK);
    }

    @PostMapping("/imagen")
    public ResponseEntity<OClienteImagenDto> addImagen(@Valid @RequestBody IClienteImagenDto iClienteImagenDto, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(clienteService.saveImagen(iClienteImagenDto), HttpStatus.CREATED);
    }

    @PutMapping("/imagen")
    public ResponseEntity<OClienteImagenDto> updateImagen(@Valid @RequestBody IClienteImagenDto iClienteImagenDto, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(clienteService.updateImagen(iClienteImagenDto), HttpStatus.OK);
    }

    @DeleteMapping("/imagen/{id}")
    public ResponseEntity<OClienteImagenDto> deleteImagen(@PathVariable int id) {
    	return new ResponseEntity<>(clienteService.deleteOneImagen(id), HttpStatus.OK);
    }
    	

    @GetMapping("/bydocumento/{tipodocid}/{documento}")
    public ResponseEntity<OClienteDto> getByDocumento(@PathVariable int tipodocid, @PathVariable String documento) {
    	return new ResponseEntity<>(clienteService.findByTipodocDocumento(tipodocid, documento), HttpStatus.OK);
    }

    @GetMapping("/byedad/{edadmin}/{edadmax}")
    public ResponseEntity<List<OClienteDto>> getByEdad(@PathVariable byte edadmin, @PathVariable byte edadmax) {
    	return new ResponseEntity<>(clienteService.findByEdad(edadmin, edadmax), HttpStatus.OK);
    }

}
