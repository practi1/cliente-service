package com.practica.clienteservice.controller;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.practica.clienteservice.dto.entrada.ICiudadDto;
import com.practica.clienteservice.dto.salida.OCiudadDto;
import com.practica.clienteservice.service.CiudadService;
import com.practica.commonsmodule.error.FaltaArgumento;
import com.practica.commonsmodule.utilis.Convert;

@RestController
@RequestMapping("/ciudades")
@RolesAllowed("admin")
public class CiudadController {

    @Autowired
    CiudadService ciudadService;
    
    @GetMapping("/all")
    public ResponseEntity<List<OCiudadDto>> list() {
    	return new ResponseEntity<>(ciudadService.findAll(), HttpStatus.OK); 
    }
    
    @GetMapping
    public ResponseEntity<Page<OCiudadDto>> getAllPages(
                        @RequestParam(defaultValue = "0") Integer pageNo, 
                        @RequestParam(defaultValue = "10") Integer pageSize,
                        @RequestParam(defaultValue = "id") String sortBy) {
    	return new ResponseEntity<>(ciudadService.getAllPages(pageNo, pageSize, sortBy), HttpStatus.OK); 
    }

    @GetMapping("/{id}")
    public ResponseEntity<OCiudadDto> get(@PathVariable int id) {
    	return new ResponseEntity<>(ciudadService.findOne(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<OCiudadDto> add(@Valid @RequestBody ICiudadDto iCiudad, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(ciudadService.save(iCiudad), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<OCiudadDto> update(@Valid @RequestBody ICiudadDto iCiudad, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(ciudadService.update(iCiudad), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<OCiudadDto> delete(@PathVariable int id) {  
    	return new ResponseEntity<>(ciudadService.deleteOne(id),HttpStatus.OK);
    }

}
