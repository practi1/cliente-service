package com.practica.clienteservice.dto.entrada;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IClienteDto implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	private int id;
	@NotEmpty(message = "Nombres no puede ser vacio o nulo")
	private String nombres;
	@NotEmpty(message = "Apellidos no puede ser vacio o nulo")
	private String apellidos;
	@Size(min = 1, max = 10, message = "Documento debe tener entre 1 y 10 caracteres")
	private String documento;
	@Range(min = 13, max = 150, message = "Edad debe estar entre 13 y 150")
	private byte edad;
	private String idImagen;
	private int idTipodoc;
    private ITipodocDto iTipodoc;
	private int idCiudad;
    private ICiudadDto iCiudad;

}
