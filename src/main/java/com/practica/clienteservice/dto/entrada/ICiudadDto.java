package com.practica.clienteservice.dto.entrada;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ICiudadDto implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	private int id;
	@NotEmpty(message = "Código no puede ser vacio o nulo")
	private String codigo;
	@NotEmpty(message = "Descripción no puede ser vacio o nulo")
	private String descripcion;

}
