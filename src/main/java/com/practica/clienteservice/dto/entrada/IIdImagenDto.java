package com.practica.clienteservice.dto.entrada;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IIdImagenDto implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	@Valid
    private List<String> ids;

}
