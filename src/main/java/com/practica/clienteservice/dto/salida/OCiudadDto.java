package com.practica.clienteservice.dto.salida;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OCiudadDto implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	private int id;
	private String codigo;
	private String descripcion;

}
