package com.practica.clienteservice.dto.salida;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OImagenDto implements Serializable {
	
	private static final long serialVersionUID = 1L;	
    private String id;
	private String titulo;
	private String mimeType;
	private String imagen64;

}
