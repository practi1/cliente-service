package com.practica.clienteservice.dto.salida;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OClienteImagenDto implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	//cliente
	private int id;
	private String nombres;
	private String apellidos;
	private String documento;
	private byte edad;
    private OTipodocDto oTipodoc;
    private OCiudadDto oCiudad;
    //imagen
    private OImagenDto oImagenDto;

}
