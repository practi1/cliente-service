package com.practica.clienteservice.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practica.clienteservice.entity.ClienteEntity;

@Repository
public interface ClienteRepository extends JpaRepository<ClienteEntity, Integer> {
	List<ClienteEntity> findByEdadBetween(byte edadMin, byte edadMax);
	Optional<ClienteEntity> findByTipodocIdAndDocumento(int tipodoc_id, String documento);
	Optional<ClienteEntity> findByIdNotAndTipodocIdAndDocumento(int id, int tipodoc_id, String documento);

}
