package com.practica.clienteservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practica.clienteservice.entity.TipodocEntity;

@Repository
public interface TipodocRepository extends JpaRepository<TipodocEntity, Integer> {
	Optional<TipodocEntity> findByCodigo(String codigo);
	Optional<TipodocEntity> findByIdNotAndCodigo(int id, String codigo);

}
