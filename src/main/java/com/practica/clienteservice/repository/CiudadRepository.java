package com.practica.clienteservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.practica.clienteservice.entity.CiudadEntity;

@Repository
public interface CiudadRepository extends JpaRepository<CiudadEntity, Integer> {
	Optional<CiudadEntity> findByCodigo(String codigo);
	Optional<CiudadEntity> findByIdNotAndCodigo(int id, String codigo);
	
}
