package com.practica.clienteservice.util;

import com.practica.clienteservice.dto.entrada.ICiudadDto;
import com.practica.clienteservice.dto.salida.OCiudadDto;
import com.practica.clienteservice.entity.CiudadEntity;

public class CiudadConverter {
	
	private CiudadConverter() {
		throw new IllegalStateException();
	}
	
	public static ICiudadDto convertCiudadEntityToICiudadDto(CiudadEntity ciudadEntity) {
		return ICiudadDto.builder()
				.id(ciudadEntity.getId())
				.codigo(ciudadEntity.getCodigo())
				.descripcion(ciudadEntity.getDescripcion())
				.build();
	}
	
	public static OCiudadDto convertCiudadEntityToOCiudadDto(CiudadEntity ciudadEntity) {
		return OCiudadDto.builder()
				.id(ciudadEntity.getId())
				.codigo(ciudadEntity.getCodigo())
				.descripcion(ciudadEntity.getDescripcion())
				.build();
	}
	
	public static CiudadEntity convertICiudadDtoToCiudadEntity(ICiudadDto iCiudadDto) {
		return CiudadEntity.builder()
				.id(iCiudadDto.getId())
				.codigo(iCiudadDto.getCodigo())
				.descripcion(iCiudadDto.getDescripcion())
				.build();
	}
	
	public static CiudadEntity convertOCiudadDtoToCiudadEntity(OCiudadDto oCiudadDto) {
		return CiudadEntity.builder()
				.id(oCiudadDto.getId())
				.codigo(oCiudadDto.getCodigo())
				.descripcion(oCiudadDto.getDescripcion())
				.build();
	}

}
