package com.practica.clienteservice.util;

import com.practica.clienteservice.dto.entrada.ITipodocDto;
import com.practica.clienteservice.dto.salida.OTipodocDto;
import com.practica.clienteservice.entity.TipodocEntity;

public class TipodocConverter {
	
	private TipodocConverter() {
		throw new IllegalStateException();
	}
	
	public static ITipodocDto convertTipodocEntityToITipodocDto(TipodocEntity tipodocEntity) {
		return ITipodocDto.builder()
				.id(tipodocEntity.getId())
				.codigo(tipodocEntity.getCodigo())
				.descripcion(tipodocEntity.getDescripcion())
				.build();
	}
	
	public static OTipodocDto convertTipodocEntityToOTipodocDto(TipodocEntity tipodocEntity) {
		return OTipodocDto.builder()
				.id(tipodocEntity.getId())
				.codigo(tipodocEntity.getCodigo())
				.descripcion(tipodocEntity.getDescripcion())
				.build();
	}
	
	public static TipodocEntity convertITipodocDtoToTipodocEntity(ITipodocDto iTipodocDto) {
		return TipodocEntity.builder()
				.id(iTipodocDto.getId())
				.codigo(iTipodocDto.getCodigo())
				.descripcion(iTipodocDto.getDescripcion())
				.build();
	}
	
	public static TipodocEntity convertOTipodocDtoToTipodocEntity(OTipodocDto oTipodocDto) {
		return TipodocEntity.builder()
				.id(oTipodocDto.getId())
				.codigo(oTipodocDto.getCodigo())
				.descripcion(oTipodocDto.getDescripcion())
				.build();
	}

}
