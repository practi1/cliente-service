package com.practica.clienteservice.util;

import com.practica.clienteservice.dto.entrada.IClienteDto;
import com.practica.clienteservice.dto.entrada.IClienteImagenDto;
import com.practica.clienteservice.dto.salida.OClienteDto;
import com.practica.clienteservice.entity.ClienteEntity;

public class ClienteConverter {
	
	private ClienteConverter() {
		throw new IllegalStateException();
	}

	public static IClienteDto convertClienteEntityToIClienteDto(ClienteEntity clienteEntity) {
		return IClienteDto.builder()
				.id(clienteEntity.getId())
				.nombres(clienteEntity.getNombres())
				.apellidos(clienteEntity.getApellidos())
				.documento(clienteEntity.getDocumento())
				.edad(clienteEntity.getEdad())
				.idImagen(clienteEntity.getIdimagen())
				.iTipodoc(TipodocConverter.convertTipodocEntityToITipodocDto(clienteEntity.getTipodoc()))
				.iCiudad(CiudadConverter.convertCiudadEntityToICiudadDto(clienteEntity.getCiudad()))
				.build();
	}

	public static OClienteDto convertClienteEntityToOClienteDto(ClienteEntity clienteEntity) {
		return OClienteDto.builder()
				.id(clienteEntity.getId())
				.nombres(clienteEntity.getNombres())
				.apellidos(clienteEntity.getApellidos())
				.documento(clienteEntity.getDocumento())
				.edad(clienteEntity.getEdad())
				.idImagen(clienteEntity.getIdimagen())
				.oTipodoc(TipodocConverter.convertTipodocEntityToOTipodocDto(clienteEntity.getTipodoc()))
				.oCiudad(CiudadConverter.convertCiudadEntityToOCiudadDto(clienteEntity.getCiudad()))
				.build();
	}

	public static ClienteEntity convertIClienteDtoToClienteEntity(IClienteDto iClienteDto) {
		return ClienteEntity.builder()
				.id(iClienteDto.getId())
				.nombres(iClienteDto.getNombres())
				.apellidos(iClienteDto.getApellidos())
				.documento(iClienteDto.getDocumento())
				.edad(iClienteDto.getEdad())
				.idimagen(iClienteDto.getIdImagen())
				.tipodoc(TipodocConverter.convertITipodocDtoToTipodocEntity(iClienteDto.getITipodoc()))
				.ciudad(CiudadConverter.convertICiudadDtoToCiudadEntity(iClienteDto.getICiudad()))
				.build();
	}

	public static ClienteEntity convertOClienteDtoToClienteEntity(OClienteDto oClienteDto) {
		return ClienteEntity.builder()
				.id(oClienteDto.getId())
				.nombres(oClienteDto.getNombres())
				.apellidos(oClienteDto.getApellidos())
				.documento(oClienteDto.getDocumento())
				.edad(oClienteDto.getEdad())
				.idimagen(oClienteDto.getIdImagen())
				.tipodoc(TipodocConverter.convertOTipodocDtoToTipodocEntity(oClienteDto.getOTipodoc()))
				.ciudad(CiudadConverter.convertOCiudadDtoToCiudadEntity(oClienteDto.getOCiudad()))
				.build();
	}

	public static IClienteDto convertIClienteImagenDtoToClienteDto(IClienteImagenDto iClienteImagenDto) {
		return IClienteDto.builder()
				.id(iClienteImagenDto.getId())
				.nombres(iClienteImagenDto.getNombres())
				.apellidos(iClienteImagenDto.getApellidos())
				.documento(iClienteImagenDto.getDocumento())
				.edad(iClienteImagenDto.getEdad())
				.idTipodoc(iClienteImagenDto.getIdTipodoc())
				.iTipodoc(iClienteImagenDto.getITipodoc())
				.idCiudad(iClienteImagenDto.getIdCiudad())
				.iCiudad(iClienteImagenDto.getICiudad())
				.build();
	}

}
