package com.practica.clienteservice.config.feign;

import com.practica.commonsmodule.error.Encontrado;
import com.practica.commonsmodule.error.FaltaArgumento;
import com.practica.commonsmodule.error.NoEncontrado;
import com.practica.commonsmodule.error.NoTerminado;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomErrorDecoder implements ErrorDecoder {

	private static final String OCURRIO_UN_ERROR_FAVOR_CONTACTAR_AL_ADMINISTRADOR = "Ocurrió un error favor contactar al administrador.";
	
    @Override
    public Exception decode(String methodKey, Response response) {

        switch (response.status()){
            case 404: //HttpStatus.NOT_FOUND
                return new NoEncontrado(response.body().toString());
            case 302: //HttpStatus.FOUND
                return new Encontrado(response.body().toString());
            case 400: //HttpStatus.BAD_REQUEST
                return new FaltaArgumento(response.body().toString());
            case 424: //HttpStatus.FAILED_DEPENDENCY
                return new NoTerminado(response.body().toString());
            default:
            	return new Exception(OCURRIO_UN_ERROR_FAVOR_CONTACTAR_AL_ADMINISTRADOR);
        }
    }
}
