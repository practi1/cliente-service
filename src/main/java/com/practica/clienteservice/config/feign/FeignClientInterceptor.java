package com.practica.clienteservice.config.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

@Configuration
public class FeignClientInterceptor implements RequestInterceptor {

  private static final String AUTHORIZATION_HEADER="Authorization";
  private static final String TOKEN_TYPE = "Bearer";

  @Override
  public void apply(RequestTemplate requestTemplate) {
	  String token = tokenExtractor();
	  if (token != null) {
		  requestTemplate.header(AUTHORIZATION_HEADER, String.format("%s %s", TOKEN_TYPE, token));
	  }
  }

  public String tokenExtractor() {
	  var requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
	  if (requestAttributes != null) {
		  var request = requestAttributes.getRequest();
		  
		  String header = request.getHeader(HttpHeaders.AUTHORIZATION);
		  if (header != null) {
		      return header.replace("Bearer ", "");
		  } else {
			  var cookie = WebUtils.getCookie(request, "auth.access_token");
			  if (cookie != null) {
				  return cookie.getValue();
			  }
		  }
	  }
	  
	  return null;
  }
  
}
