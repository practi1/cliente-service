package com.practica.clienteservice.feign.fallback;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.practica.clienteservice.dto.entrada.IIdImagenDto;
import com.practica.clienteservice.dto.entrada.IImagenDto;
import com.practica.clienteservice.dto.salida.OImagenDto;
import com.practica.clienteservice.feign.ImagenFeign;

@Component
public class ImagenHystrixFallbackFactory implements ImagenFeign {
	@Override
	public ResponseEntity<List<OImagenDto>> list() {
		var oImagenDto = OImagenDto.builder().build();
		List<OImagenDto> nImagenesDto = new ArrayList<>();
		nImagenesDto.add(oImagenDto);
    	return new ResponseEntity<>(nImagenesDto, HttpStatus.OK); 	
	}

	@Override
	public ResponseEntity<Page<OImagenDto>> getAllPages(Integer pageNo, Integer pageSize, String sortBy) {
		var oImagenDto = OImagenDto.builder().build();
		List<OImagenDto> nImagenesDto = new ArrayList<>();
		nImagenesDto.add(oImagenDto);
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy)); 
		Page<OImagenDto> imagenesPageDto = new PageImpl<>(nImagenesDto, paging, nImagenesDto.size());
    	return new ResponseEntity<>(imagenesPageDto, HttpStatus.OK);     	
	}

	@Override
	public ResponseEntity<OImagenDto> get(String id) {
		var oImagenDto = OImagenDto.builder().build();
    	return new ResponseEntity<>(oImagenDto, HttpStatus.OK); 
	}

	@Override
	public ResponseEntity<OImagenDto> add(IImagenDto iImagen) {
		var oImagenDto = OImagenDto.builder().build();
    	return new ResponseEntity<>(oImagenDto, HttpStatus.CREATED); 
	}

	@Override
	public ResponseEntity<OImagenDto> addFile(MultipartFile imagenfile) throws IOException {
		var oImagenDto = OImagenDto.builder().build();
    	return new ResponseEntity<>(oImagenDto, HttpStatus.CREATED); 
	}

	@Override
	public ResponseEntity<OImagenDto> update(IImagenDto iImagen) {
		var oImagenDto = OImagenDto.builder().build();
    	return new ResponseEntity<>(oImagenDto, HttpStatus.OK); 
	}

	@Override
	public ResponseEntity<OImagenDto> updateFile(String id, MultipartFile imagenfile) throws IOException {
		var oImagenDto = OImagenDto.builder().build();
    	return new ResponseEntity<>(oImagenDto, HttpStatus.OK); 
	}

	@Override
	public ResponseEntity<OImagenDto> delete(String id) {
		var oImagenDto = OImagenDto.builder().build();
    	return new ResponseEntity<>(oImagenDto, HttpStatus.OK); 
	}

	@Override
    public ResponseEntity<List<OImagenDto>> listById(IIdImagenDto listIdt) {
		var oImagenDto = OImagenDto.builder().build();
		List<OImagenDto> nImagenesDto = new ArrayList<>();
		nImagenesDto.add(oImagenDto);
    	return new ResponseEntity<>(nImagenesDto, HttpStatus.OK); 	
	}
	
}
