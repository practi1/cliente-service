package com.practica.clienteservice.feign;

import java.io.IOException;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.practica.clienteservice.dto.entrada.IIdImagenDto;
import com.practica.clienteservice.dto.entrada.IImagenDto;
import com.practica.clienteservice.dto.salida.OImagenDto;
import com.practica.clienteservice.feign.fallback.ImagenHystrixFallbackFactory;

@FeignClient(name = "${feign.services.imagenService}", fallback = ImagenHystrixFallbackFactory.class)
public interface ImagenFeign {
	static final String PREFIJO_URL = "/imagenes";
    
    @GetMapping(PREFIJO_URL + "/all")
    public ResponseEntity<List<OImagenDto>> list();
    
    @GetMapping(PREFIJO_URL)
    public ResponseEntity<Page<OImagenDto>> getAllPages(
                        @RequestParam(defaultValue = "0") Integer pageNo, 
                        @RequestParam(defaultValue = "10") Integer pageSize,
                        @RequestParam(defaultValue = "id") String sortBy);

    @GetMapping(PREFIJO_URL + "/{id}")
    public ResponseEntity<OImagenDto> get(@PathVariable String id);

    @PostMapping(PREFIJO_URL)
    public ResponseEntity<OImagenDto> add(@RequestBody IImagenDto iImagen);

    @PostMapping(PREFIJO_URL + "/file")
    public ResponseEntity<OImagenDto> addFile(@RequestPart("imagenfile") MultipartFile imagenfile) throws IOException;

    @PutMapping(PREFIJO_URL)
    public ResponseEntity<OImagenDto> update(@RequestBody IImagenDto iImagen);

    @PutMapping(PREFIJO_URL + "/file")
    public ResponseEntity<OImagenDto> updateFile(@RequestParam String id, @RequestPart("imagen") MultipartFile imagenfile) throws IOException;

    @DeleteMapping(PREFIJO_URL + "/{id}")
    public ResponseEntity<OImagenDto> delete(@PathVariable String id);

    @PostMapping(PREFIJO_URL + "/ids")
    public ResponseEntity<List<OImagenDto>> listById(@RequestBody IIdImagenDto listId);

}
