package com.practica.clienteservice.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "tipodoc", indexes = {@Index(columnList = "codigo, actived", unique = true)})
@SQLDelete(sql = "UPDATE tipodoc SET actived = NULL WHERE id=?")
@Where(clause = "actived=true")
public class TipodocEntity {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int id;
	
	@Column(nullable=false, unique=true)
	private String codigo;
	
	@Column(nullable=false)
	private String descripcion;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "tipodoc")
    private List<ClienteEntity> clientes;

	@Column
	@Builder.Default
	private boolean actived = Boolean.TRUE;

}
