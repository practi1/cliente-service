package com.practica.clienteservice.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "cliente",
		indexes = {@Index(columnList = "idtipodoc, documento, actived", unique = true),
					@Index(columnList = "edad, actived")})
@SQLDelete(sql = "UPDATE cliente SET actived = NULL WHERE id=?")
@Where(clause = "actived=true")
public class ClienteEntity {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int id;
	
	@Column(nullable=false)
	private String nombres;
	
	@Column(nullable=false)
	private String apellidos;
	
	@Column(nullable=false)
	private String documento;
	
	@Column(nullable=false)
	private byte edad;
	
	@Column
	private String idimagen;
    
	//si usa .LAZY para obtener la información debe llamar .size() o .getCodigo() para que se haga la consulta
    @ManyToOne(fetch = FetchType.EAGER, optional = false)  
    @JoinColumn(name = "idtipodoc", nullable = false)
    private TipodocEntity tipodoc;
    
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "idciudad", nullable = false)
    private CiudadEntity ciudad;

	@Column
	@Builder.Default
	private boolean actived = Boolean.TRUE;

}
