package com.practica.clienteservice.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.practica.clienteservice.dto.entrada.IClienteDto;
import com.practica.clienteservice.dto.entrada.IClienteImagenDto;
import com.practica.clienteservice.dto.salida.OClienteDto;
import com.practica.clienteservice.dto.salida.OClienteImagenDto;

public interface ClienteService {
	public List<OClienteDto> findAll();
	public Page<OClienteDto> getAllPages(Integer pageNo, Integer pageSize, String sortBy);
	public OClienteDto findOne(Integer id);
	public OClienteDto save(IClienteDto iCliente);
	public OClienteDto update(IClienteDto iCliente);
	public OClienteDto deleteOne(Integer id);    
    //endpoint requeridos
	public Page<OClienteImagenDto> getAllPagesImagen(Integer pageNo, Integer pageSize, String sortBy);
	public OClienteImagenDto findOneImagen(Integer id);
	public OClienteImagenDto saveImagen(IClienteImagenDto iClienteImagenDto);
	public OClienteImagenDto updateImagen(IClienteImagenDto iClienteImagenDto);
	public OClienteImagenDto deleteOneImagen(Integer id);  
	public OClienteDto findByTipodocDocumento(int tipodocid, String documento);
	public List<OClienteDto> findByEdad(byte edadMin, byte edadMax);

}
