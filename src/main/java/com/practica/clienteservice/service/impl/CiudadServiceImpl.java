package com.practica.clienteservice.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.practica.clienteservice.dto.entrada.ICiudadDto;
import com.practica.clienteservice.dto.salida.OCiudadDto;
import com.practica.clienteservice.entity.CiudadEntity;
import com.practica.clienteservice.repository.CiudadRepository;
import com.practica.clienteservice.service.CiudadService;
import com.practica.clienteservice.util.CiudadConverter;
import com.practica.commonsmodule.error.Encontrado;
import com.practica.commonsmodule.error.NoEncontrado;
import com.practica.commonsmodule.utilis.FormatString;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service(value = "ciudadService")
public class CiudadServiceImpl implements CiudadService {
	
	private static final String RECURSO = "ciudad";
	private static final String IDENTIFICADOR = "código";
	
	@Autowired
	private CiudadRepository ciudadRepository;

	@Override
	public List<OCiudadDto> findAll() {
		List<OCiudadDto> nCiudadesDto = new ArrayList<>();
		List<CiudadEntity> nCiudades = ciudadRepository.findAll();
		for(CiudadEntity ciudad : nCiudades) {
			nCiudadesDto.add(CiudadConverter.convertCiudadEntityToOCiudadDto(ciudad));
		}
		log.info("se consulta listado de ciudades");
        return nCiudadesDto;
	}

	@Override
	public Page<OCiudadDto> getAllPages(Integer pageNo, Integer pageSize, String sortBy){
		List<OCiudadDto> nCiudadesDto = new ArrayList<>();
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy)); 
        Page<CiudadEntity> nCiudades = ciudadRepository.findAll(paging);
		for(CiudadEntity ciudad : nCiudades) {
			nCiudadesDto.add(CiudadConverter.convertCiudadEntityToOCiudadDto(ciudad));
		}
		log.info("se consulta paginado de ciudades");
		return new PageImpl<>(nCiudadesDto, nCiudades.getPageable(), nCiudades.getTotalElements());
	}

	@Override
	public OCiudadDto findOne(Integer id) {
		Optional<CiudadEntity> nCiudadOpt = ciudadRepository.findById(id);
		if (nCiudadOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id.toString()));
		}
		log.info("se consulta ciudad {}", id);
        return CiudadConverter.convertCiudadEntityToOCiudadDto(nCiudadOpt.get());
	}

	@Override
	public OCiudadDto save(ICiudadDto iCiudad) {	
		if (iCiudad.getId() != 0) {
			throw new Encontrado(FormatString.seEnvioString("id"));
		}
		if (ciudadRepository.findByCodigo(iCiudad.getCodigo()).isPresent()) {
			throw new Encontrado(FormatString.yaExisteString(RECURSO, IDENTIFICADOR, iCiudad.getCodigo()));
		}
		CiudadEntity nCiudad = CiudadConverter.convertICiudadDtoToCiudadEntity(iCiudad);
		nCiudad = ciudadRepository.save(nCiudad);
		log.info("se crea ciudad con el id: {}",  nCiudad.getId());
        return CiudadConverter.convertCiudadEntityToOCiudadDto(nCiudad);
	}

	@Override
	public OCiudadDto update(ICiudadDto iCiudad) {	
		if (ciudadRepository.findById(iCiudad.getId()).isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", Integer.toString(iCiudad.getId())));
		}
		if (ciudadRepository.findByIdNotAndCodigo(iCiudad.getId(), iCiudad.getCodigo()).isPresent()) {
			throw new Encontrado(FormatString.yaExisteString(RECURSO, IDENTIFICADOR, iCiudad.getCodigo()));
		}
		CiudadEntity nCiudad = CiudadConverter.convertICiudadDtoToCiudadEntity(iCiudad);
		nCiudad = ciudadRepository.save(nCiudad);
		log.info("se actualiza ciudad con el id: {}", nCiudad.getId());
        return CiudadConverter.convertCiudadEntityToOCiudadDto(nCiudad);
	}

	@Override
	public OCiudadDto deleteOne(Integer id) {
		Optional<CiudadEntity> nCiudadOpt = ciudadRepository.findById(id);
		if (nCiudadOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id.toString()));
		}
		ciudadRepository.deleteById(id);
		log.info("se elimina ciudad con el id: {}", id);
		return CiudadConverter.convertCiudadEntityToOCiudadDto(nCiudadOpt.get());
	}

}
