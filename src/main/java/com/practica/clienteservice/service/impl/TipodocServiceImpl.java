package com.practica.clienteservice.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.practica.clienteservice.dto.entrada.ITipodocDto;
import com.practica.clienteservice.dto.salida.OTipodocDto;
import com.practica.clienteservice.entity.TipodocEntity;
import com.practica.clienteservice.repository.TipodocRepository;
import com.practica.clienteservice.service.TipodocService;
import com.practica.clienteservice.util.TipodocConverter;
import com.practica.commonsmodule.error.Encontrado;
import com.practica.commonsmodule.error.NoEncontrado;
import com.practica.commonsmodule.utilis.FormatString;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service(value = "tipodocService")
public class TipodocServiceImpl implements TipodocService {
	
	private static final String RECURSO = "tipo de documento";
	private static final String IDENTIFICADOR = "código";
	
	@Autowired
	private TipodocRepository tipodocRepository;

	@Override
	public List<OTipodocDto> findAll() {
		List<OTipodocDto> nTipodocsDto = new ArrayList<>();
		List<TipodocEntity> nTipodocs = tipodocRepository.findAll();
		for(TipodocEntity tipodoc : nTipodocs) {
			nTipodocsDto.add(TipodocConverter.convertTipodocEntityToOTipodocDto(tipodoc));
		}
		log.info("se consulta listado de tipo de documentos");
        return nTipodocsDto;
	}

	@Override
	public Page<OTipodocDto> getAllPages(Integer pageNo, Integer pageSize, String sortBy){
		List<OTipodocDto> nTipodocsDto = new ArrayList<>();
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy)); 
        Page<TipodocEntity> nTipodocs = tipodocRepository.findAll(paging);
		for(TipodocEntity tipodoc : nTipodocs) {
			nTipodocsDto.add(TipodocConverter.convertTipodocEntityToOTipodocDto(tipodoc));
		}
		log.info("se consulta paginado de tipo de documentos");
		return new PageImpl<>(nTipodocsDto, nTipodocs.getPageable(), nTipodocs.getTotalElements());
	}

	@Override
	public OTipodocDto findOne(Integer id) {
		Optional<TipodocEntity> nTipodocOpt = tipodocRepository.findById(id);
		if (nTipodocOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id.toString()));
		}
		log.info("se consulta tipo de documento {}", id);
        return TipodocConverter.convertTipodocEntityToOTipodocDto(nTipodocOpt.get());
	}

	@Override
	public OTipodocDto save(ITipodocDto iTipodoc) {	
		if (iTipodoc.getId() != 0) {
			throw new Encontrado(FormatString.seEnvioString("id"));
		}
		if (tipodocRepository.findByCodigo(iTipodoc.getCodigo()).isPresent()) {
			throw new Encontrado(FormatString.yaExisteString(RECURSO, IDENTIFICADOR, iTipodoc.getCodigo()));
		}
		TipodocEntity nTipodoc = TipodocConverter.convertITipodocDtoToTipodocEntity(iTipodoc);
		nTipodoc = tipodocRepository.save(nTipodoc);
		log.info("se crea tipo de documento con el id: {}",  nTipodoc.getId());
        return TipodocConverter.convertTipodocEntityToOTipodocDto(nTipodoc);
	}

	@Override
	public OTipodocDto update(ITipodocDto iTipodoc) {		
		if (tipodocRepository.findById(iTipodoc.getId()).isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", Integer.toString(iTipodoc.getId())));
		}
		if (tipodocRepository.findByIdNotAndCodigo(iTipodoc.getId(), iTipodoc.getCodigo()).isPresent()) {
			throw new Encontrado(FormatString.yaExisteString(RECURSO, IDENTIFICADOR, iTipodoc.getCodigo()));
		}
		TipodocEntity nTipodoc = TipodocConverter.convertITipodocDtoToTipodocEntity(iTipodoc);
		nTipodoc = tipodocRepository.save(nTipodoc);
		log.info("se actualiza tipo de documento con el id: {}", nTipodoc.getId());
        return TipodocConverter.convertTipodocEntityToOTipodocDto(nTipodoc);
	}

	@Override
	public OTipodocDto deleteOne(Integer id) {
		Optional<TipodocEntity> nTipodocOpt = tipodocRepository.findById(id);
		if (nTipodocOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id.toString()));
		}
		tipodocRepository.deleteById(id);
		log.info("se elimina tipo de documento con el id: {}", id);
		return TipodocConverter.convertTipodocEntityToOTipodocDto(nTipodocOpt.get());
	}

}
