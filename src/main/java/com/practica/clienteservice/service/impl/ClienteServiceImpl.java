package com.practica.clienteservice.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.practica.clienteservice.dto.entrada.IClienteDto;
import com.practica.clienteservice.dto.entrada.IClienteImagenDto;
import com.practica.clienteservice.dto.entrada.IIdImagenDto;
import com.practica.clienteservice.dto.entrada.IImagenDto;
import com.practica.clienteservice.dto.salida.OClienteDto;
import com.practica.clienteservice.dto.salida.OClienteImagenDto;
import com.practica.clienteservice.dto.salida.OImagenDto;
import com.practica.clienteservice.entity.ClienteEntity;
import com.practica.clienteservice.feign.ImagenFeign;
import com.practica.clienteservice.repository.CiudadRepository;
import com.practica.clienteservice.repository.ClienteRepository;
import com.practica.clienteservice.repository.TipodocRepository;
import com.practica.clienteservice.service.ClienteService;
import com.practica.clienteservice.util.CiudadConverter;
import com.practica.clienteservice.util.ClienteConverter;
import com.practica.clienteservice.util.TipodocConverter;
import com.practica.commonsmodule.error.Encontrado;
import com.practica.commonsmodule.error.NoEncontrado;
import com.practica.commonsmodule.error.NoTerminado;
import com.practica.commonsmodule.utilis.FormatString;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service(value = "clienteService")
public class ClienteServiceImpl implements ClienteService {

	private static final String RECURSO = "cliente";
	private static final String RECURSO_CIUDAD = "ciudad";
	private static final String RECURSO_TIPODOC = "tipodoc";
	private static final String RECURSO_IMAGEN = "imagen";
	private static final String IDENTIFICADOR = "documento";
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private CiudadRepository ciudadRepository;
	
	@Autowired
	private TipodocRepository tipodocRepository;
	
	@Autowired
	ImagenFeign imagenFeign;

	@Override
	public List<OClienteDto> findAll() {
		List<OClienteDto> nClientesDto = new ArrayList<>();
		List<ClienteEntity> nClientes = clienteRepository.findAll();
		for(ClienteEntity cliente : nClientes) {
			nClientesDto.add(ClienteConverter.convertClienteEntityToOClienteDto(cliente));
		}
		log.info("se consulta listado de clientes");
        return nClientesDto;
	}

	@Override
	public Page<OClienteDto> getAllPages(Integer pageNo, Integer pageSize, String sortBy){
		List<OClienteDto> nClientesDto = new ArrayList<>();
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy)); 
        Page<ClienteEntity> nClientes = clienteRepository.findAll(paging);
		for(ClienteEntity cliente : nClientes) {
			nClientesDto.add(ClienteConverter.convertClienteEntityToOClienteDto(cliente));
		}
		log.info("se consulta paginado de clientes");
		return new PageImpl<>(nClientesDto, nClientes.getPageable(), nClientes.getTotalElements());
	}
	
	@Override
	public OClienteDto findOne(Integer id) {
		Optional<ClienteEntity> nClienteOpt = clienteRepository.findById(id);
		if (nClienteOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id.toString()));
		}
		log.info("se consulta cliente {}", id);
        return ClienteConverter.convertClienteEntityToOClienteDto(nClienteOpt.get());
	}

	@Override
	public OClienteDto save(IClienteDto iCliente) {	
		if (iCliente.getId() != 0) {
			throw new Encontrado(FormatString.seEnvioString("id"));
		}

		cargarTipodoc(iCliente);
		cargarCiudad(iCliente);
		
		if (clienteRepository.findByTipodocIdAndDocumento(iCliente.getIdTipodoc(), iCliente.getDocumento()).isPresent()) {
			throw new Encontrado(FormatString.yaExisteString(RECURSO, IDENTIFICADOR, iCliente.getDocumento()));
		}
		
		ClienteEntity nCliente = ClienteConverter.convertIClienteDtoToClienteEntity(iCliente);
		nCliente = clienteRepository.save(nCliente);
		log.info("se crea cliente con el id: {}",  nCliente.getId());
        return ClienteConverter.convertClienteEntityToOClienteDto(nCliente);
	}

	@Override
	public OClienteDto update(IClienteDto iCliente) {	
		if (clienteRepository.findById(iCliente.getId()).isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", Integer.toString(iCliente.getId())));
		}
		if (clienteRepository.findByIdNotAndTipodocIdAndDocumento(iCliente.getId(), iCliente.getIdTipodoc(), iCliente.getDocumento()).isPresent()) {
			throw new Encontrado(FormatString.yaExisteString(RECURSO, IDENTIFICADOR, iCliente.getDocumento()));
		}

		cargarTipodoc(iCliente);
		cargarCiudad(iCliente);
		
		ClienteEntity nCliente = ClienteConverter.convertIClienteDtoToClienteEntity(iCliente);
		nCliente = clienteRepository.save(nCliente);
		log.info("se actualiza cliente con el id: {}", nCliente.getId());
        return ClienteConverter.convertClienteEntityToOClienteDto(nCliente);
	}

	@Override
	public OClienteDto deleteOne(Integer id) {
		Optional<ClienteEntity> nClienteOpt = clienteRepository.findById(id);
		if (nClienteOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id.toString()));
		}
		clienteRepository.deleteById(id);
		log.info("se elimina cliente con el id: {}", id);
		return ClienteConverter.convertClienteEntityToOClienteDto(nClienteOpt.get());
	}
    
    //endpoint requeridos

	@Override
	public Page<OClienteImagenDto> getAllPagesImagen(Integer pageNo, Integer pageSize, String sortBy){
		List<OClienteImagenDto> nClientesDto = new ArrayList<>();
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy)); 
        Page<ClienteEntity> nClientes = clienteRepository.findAll(paging);
        
		List<String> idImagenes = new ArrayList<>();
		for(ClienteEntity cliente : nClientes) {
			idImagenes.add(cliente.getIdimagen());
		}
		var iIdImagenDto = IIdImagenDto.builder().ids(idImagenes).build();
        var oImagenesDtoResponse = imagenFeign.listById(iIdImagenDto);
		List<OImagenDto> oImagenesDto = null; 
		if (oImagenesDtoResponse.getStatusCode() == HttpStatus.OK) {
			oImagenesDto = oImagenesDtoResponse.getBody();
		} else {
			throw new NoTerminado(FormatString.noOperacionString("encontró", RECURSO_IMAGEN));
		}
        
		for(ClienteEntity cliente : nClientes) {
			OImagenDto imagen = null;
			for(OImagenDto imagenDto : oImagenesDto) {
				if (cliente.getIdimagen().equals(imagenDto.getId())) {
					imagen = imagenDto;
				}
			}
			nClientesDto.add(OClienteImagenDto.builder()
					.id(cliente.getId())
					.nombres(cliente.getNombres())
					.apellidos(cliente.getApellidos())
					.documento(cliente.getDocumento())
					.edad(cliente.getEdad())
					.oTipodoc(TipodocConverter.convertTipodocEntityToOTipodocDto(cliente.getTipodoc()))
					.oCiudad(CiudadConverter.convertCiudadEntityToOCiudadDto(cliente.getCiudad()))
					.oImagenDto(imagen)
					.build());
		}
		log.info("se consulta paginado de clientes con imagen");
		return new PageImpl<>(nClientesDto, nClientes.getPageable(), nClientes.getTotalElements());
	}
	
	@Override
	public OClienteImagenDto findOneImagen(Integer id) {
		Optional<ClienteEntity> nClienteOpt = clienteRepository.findById(id);
		if (nClienteOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id.toString()));
		}

		OImagenDto oImagenDto = null; 
		if ((nClienteOpt.get().getIdimagen() != null) && (!nClienteOpt.get().getIdimagen().isBlank())) {
			ResponseEntity<OImagenDto> oImagenDtoResponse = imagenFeign.get(nClienteOpt.get().getIdimagen());
			if (oImagenDtoResponse.getStatusCode() == HttpStatus.OK) {
				oImagenDto = oImagenDtoResponse.getBody();
			} else {
				throw new NoTerminado(FormatString.noOperacionString("encontró", RECURSO_IMAGEN));
			}
		}
		
		var oClienteDto = ClienteConverter.convertClienteEntityToOClienteDto(nClienteOpt.get());
		log.info("se consulta cliente con imagen {}", id);
		return OClienteImagenDto.builder()
				.id(oClienteDto.getId())
				.nombres(oClienteDto.getNombres())
				.apellidos(oClienteDto.getApellidos())
				.documento(oClienteDto.getDocumento())
				.edad(oClienteDto.getEdad())
				.oTipodoc(oClienteDto.getOTipodoc())
				.oCiudad(oClienteDto.getOCiudad())
				.oImagenDto(oImagenDto)
				.build();
	}

	@Override
	@Transactional
	public OClienteImagenDto saveImagen(IClienteImagenDto iClienteImagenDto) {
		var iCliente = ClienteConverter.convertIClienteImagenDtoToClienteDto(iClienteImagenDto);
		var iImagen = iClienteImagenDto.getIImagenDto();
		
		if (iCliente.getId() != 0) {
			throw new Encontrado(FormatString.seEnvioString("id"));
		}

		cargarTipodoc(iCliente);
		cargarCiudad(iCliente);
		
		if (clienteRepository.findByTipodocIdAndDocumento(iCliente.getIdTipodoc(), iCliente.getDocumento()).isPresent()) {
			throw new Encontrado(FormatString.yaExisteString(RECURSO, IDENTIFICADOR, iCliente.getDocumento()));
		}

		OImagenDto oImagenDto = null; 
		if (iImagen != null) {
			oImagenDto = crearImagen(iImagen);
			if (oImagenDto != null) {
				iCliente.setIdImagen(oImagenDto.getId());
			}
		}
		
		ClienteEntity nCliente = ClienteConverter.convertIClienteDtoToClienteEntity(iCliente);
		try {
			nCliente = clienteRepository.save(nCliente);
		} catch (Exception e) {
			if ((iCliente.getIdImagen() != null) && (!iCliente.getIdImagen().isBlank())) {
				imagenFeign.delete(iCliente.getIdImagen());
			}
		}
		var oClienteDto = ClienteConverter.convertClienteEntityToOClienteDto(nCliente);
		log.info("se crea cliente con imagen con el id: {}",  nCliente.getId());
		return OClienteImagenDto.builder()
				.id(oClienteDto.getId())
				.nombres(oClienteDto.getNombres())
				.apellidos(oClienteDto.getApellidos())
				.documento(oClienteDto.getDocumento())
				.edad(oClienteDto.getEdad())
				.oTipodoc(oClienteDto.getOTipodoc())
				.oCiudad(oClienteDto.getOCiudad())
				.oImagenDto(oImagenDto)
				.build();
	}

	@Override
	public OClienteImagenDto updateImagen(IClienteImagenDto iClienteImagenDto) {
		var iCliente = ClienteConverter.convertIClienteImagenDtoToClienteDto(iClienteImagenDto);
		var iImagen = iClienteImagenDto.getIImagenDto();
		
		if (clienteRepository.findById(iCliente.getId()).isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", Integer.toString(iCliente.getId())));
		}
		if (clienteRepository.findByIdNotAndTipodocIdAndDocumento(iCliente.getId(), iCliente.getIdTipodoc(), iCliente.getDocumento()).isPresent()) {
			throw new Encontrado(FormatString.yaExisteString(RECURSO, IDENTIFICADOR, iCliente.getDocumento()));
		}

		cargarTipodoc(iCliente);
		cargarCiudad(iCliente);

		OImagenDto oImagenDto = null; 
		if (iImagen != null) {
			oImagenDto = actualizarImagen(iImagen);
			if (oImagenDto != null) {
				iCliente.setIdImagen(oImagenDto.getId());
			}
		}
		
		var nCliente = ClienteConverter.convertIClienteDtoToClienteEntity(iCliente);
		nCliente = clienteRepository.save(nCliente);
		var oClienteDto = ClienteConverter.convertClienteEntityToOClienteDto(nCliente);
		log.info("se actualiza cliente con imagen con el id: {}", nCliente.getId());
		return OClienteImagenDto.builder()
				.id(oClienteDto.getId())
				.nombres(oClienteDto.getNombres())
				.apellidos(oClienteDto.getApellidos())
				.documento(oClienteDto.getDocumento())
				.edad(oClienteDto.getEdad())
				.oTipodoc(oClienteDto.getOTipodoc())
				.oCiudad(oClienteDto.getOCiudad())
				.oImagenDto(oImagenDto)
				.build();
	}

	@Override
	@Transactional
	public OClienteImagenDto deleteOneImagen(Integer id) {
		Optional<ClienteEntity> nClienteOpt = clienteRepository.findById(id);
		if (nClienteOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id.toString()));
		}
		var oClienteDto = ClienteConverter.convertClienteEntityToOClienteDto(nClienteOpt.get());
		ResponseEntity<OImagenDto> oImagenDtofind = null;
		OImagenDto oImagenDto = null; 
		ResponseEntity<OImagenDto> oImagenDtoResponse = null;
		if ((oClienteDto.getIdImagen() != null) && (!oClienteDto.getIdImagen().isBlank())) {
			oImagenDtofind = imagenFeign.get(oClienteDto.getIdImagen());
			if (oImagenDtofind.getStatusCode() == HttpStatus.OK) {
				oImagenDtoResponse = imagenFeign.delete(oClienteDto.getIdImagen());
				if (oImagenDtoResponse.getStatusCode() == HttpStatus.OK) {
					oImagenDto = oImagenDtoResponse.getBody();
				} else {
					throw new NoTerminado(FormatString.noOperacionString("borró", RECURSO_IMAGEN));
				}
			}
		}
			
		try {
			clienteRepository.deleteById(id);
		} catch(Exception e) {
			if (oImagenDtofind != null) {
				var imagenDtofind = oImagenDtofind.getBody();
				if (imagenDtofind != null ) {
					imagenFeign.add(new IImagenDto("", imagenDtofind.getTitulo(), imagenDtofind.getMimeType(), imagenDtofind.getImagen64()));
				}
			}
		}
		log.info("se elimina cliente con imagen con el id: {}", id);
		return OClienteImagenDto.builder()
				.id(oClienteDto.getId())
				.nombres(oClienteDto.getNombres())
				.apellidos(oClienteDto.getApellidos())
				.documento(oClienteDto.getDocumento())
				.edad(oClienteDto.getEdad())
				.oTipodoc(oClienteDto.getOTipodoc())
				.oCiudad(oClienteDto.getOCiudad())
				.oImagenDto(oImagenDto)
				.build();
	}

	@Override
	public OClienteDto findByTipodocDocumento(int tipodocid, String documento) {
		Optional<ClienteEntity> nCliente = clienteRepository.findByTipodocIdAndDocumento(tipodocid, documento);
		if (nCliente.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, IDENTIFICADOR, documento));
		}
		log.info("se consulta cliente documento {} {}", tipodocid, documento);
        return ClienteConverter.convertClienteEntityToOClienteDto(nCliente.get());
	}

	@Override
	public List<OClienteDto> findByEdad(byte edadMin, byte edadMax) {
		List<OClienteDto> nClientesDto = new ArrayList<>();
		List<ClienteEntity> nClientes = clienteRepository.findByEdadBetween(edadMin, edadMax);
		for(ClienteEntity cliente : nClientes) {
			nClientesDto.add(ClienteConverter.convertClienteEntityToOClienteDto(cliente));
		}
		log.info("se consulta cliente edades entre {} {}", edadMin, edadMax);
        return nClientesDto;
	}
	
	private IClienteDto cargarTipodoc(IClienteDto iCliente) {		
		if (iCliente.getIdTipodoc() != 0) {
			var tipodocOptional = tipodocRepository.findById(iCliente.getIdTipodoc());
			if (tipodocOptional.isPresent()) {
				iCliente.setITipodoc(TipodocConverter.convertTipodocEntityToITipodocDto(tipodocOptional.get()));
			} else {
				throw new NoEncontrado(FormatString.noExisteString(RECURSO_TIPODOC, "id", Integer.toString(iCliente.getIdTipodoc())));
			}
		} else {
			if (iCliente.getITipodoc() == null) {
				throw new NoEncontrado(FormatString.noSeEnvioString("tipo de documento"));
			} else if (tipodocRepository.findById(iCliente.getITipodoc().getId()).isEmpty()) {
				throw new NoEncontrado(FormatString.noExisteString(RECURSO_TIPODOC, "id", Integer.toString(iCliente.getITipodoc().getId())));
			} else {
				iCliente.setIdTipodoc(iCliente.getITipodoc().getId());
			}
		}
		return iCliente;
	}
	
	private IClienteDto cargarCiudad(IClienteDto iCliente) {				
		if (iCliente.getIdCiudad() != 0) {
			var iCiudadOptional = ciudadRepository.findById(iCliente.getIdCiudad());
			if (iCiudadOptional.isPresent()) {
				iCliente.setICiudad(CiudadConverter.convertCiudadEntityToICiudadDto(iCiudadOptional.get()));
			} else {
				throw new NoEncontrado(FormatString.noExisteString(RECURSO_CIUDAD, "id", Integer.toString(iCliente.getIdCiudad())));
			}
		} else {
			if (iCliente.getICiudad() == null) {
				throw new NoEncontrado(FormatString.noSeEnvioString(RECURSO_CIUDAD));
			} else if (ciudadRepository.findById(iCliente.getICiudad().getId()).isEmpty()) {
				throw new NoEncontrado(FormatString.noExisteString(RECURSO_CIUDAD, "id", Integer.toString(iCliente.getICiudad().getId())));
			} else {
				iCliente.setIdCiudad(iCliente.getICiudad().getId());
			}
		}
		return iCliente;
	}
	
	private OImagenDto crearImagen(IImagenDto iImagen) {
		ResponseEntity<OImagenDto> oImagenDtoResponse = null;
		if ((iImagen.getId() != null) && (!iImagen.getId().isBlank())) {
			oImagenDtoResponse = imagenFeign.get(iImagen.getId());
			if (oImagenDtoResponse.getStatusCode() != HttpStatus.OK) {
				throw new NoTerminado(FormatString.noOperacionString("encontró", RECURSO_IMAGEN));
			}
		} else {
			oImagenDtoResponse = imagenFeign.add(iImagen);
			if (oImagenDtoResponse.getStatusCode() != HttpStatus.CREATED) {
				throw new NoTerminado(FormatString.noOperacionString("creó", RECURSO_IMAGEN));
			}
		}
		return oImagenDtoResponse.getBody();
	}
	
	private OImagenDto actualizarImagen(IImagenDto iImagen) {
		ResponseEntity<OImagenDto> oImagenDtoResponse = null;
		if ((iImagen.getId() == null) || (iImagen.getId().isBlank())) {
			oImagenDtoResponse = imagenFeign.add(iImagen);
			if (oImagenDtoResponse.getStatusCode() != HttpStatus.CREATED) {
				throw new NoTerminado(FormatString.noOperacionString("creó", RECURSO_IMAGEN));
			}
		} else {
			ResponseEntity<OImagenDto> oImagenDtofind = imagenFeign.get(iImagen.getId());
			if (oImagenDtofind.getStatusCode() == HttpStatus.OK) {
				oImagenDtoResponse = imagenFeign.update(iImagen);
				if (oImagenDtoResponse.getStatusCode() != HttpStatus.OK) {
					throw new NoTerminado(FormatString.noOperacionString("actualizó", RECURSO_IMAGEN));				
				}
			} else {
				throw new NoEncontrado(FormatString.noExisteString(RECURSO_IMAGEN, "id", iImagen.getId()));
			}
		}
		return oImagenDtoResponse.getBody();
	}

}
