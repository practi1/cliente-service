package com.practica.clienteservice.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.practica.clienteservice.dto.entrada.ICiudadDto;
import com.practica.clienteservice.dto.salida.OCiudadDto;

public interface CiudadService {
	public List<OCiudadDto> findAll();
	public Page<OCiudadDto> getAllPages(Integer pageNo, Integer pageSize, String sortBy);
	public OCiudadDto findOne(Integer id);
	public OCiudadDto save(ICiudadDto iCiudad);
	public OCiudadDto update(ICiudadDto iCiudad);
	public OCiudadDto deleteOne(Integer id);

}
