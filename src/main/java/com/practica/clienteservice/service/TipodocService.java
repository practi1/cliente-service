package com.practica.clienteservice.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.practica.clienteservice.dto.entrada.ITipodocDto;
import com.practica.clienteservice.dto.salida.OTipodocDto;

public interface TipodocService {
	public List<OTipodocDto> findAll();
	public Page<OTipodocDto> getAllPages(Integer pageNo, Integer pageSize, String sortBy);
	public OTipodocDto findOne(Integer id);
	public OTipodocDto save(ITipodocDto iTipodoc);
	public OTipodocDto update(ITipodocDto iTipodoc);
	public OTipodocDto deleteOne(Integer id);

}
