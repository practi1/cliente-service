CREATE DATABASE IF NOT EXISTS `practica_clientes`;

USE `practica_clientes`;

CREATE TABLE IF NOT EXISTS `ciudad` (
  `id` int NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `actived` boolean DEFAULT true,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_k3iyhytdcvm6lq5x3o5bc1e9i` (`codigo`,`actived`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `tipodoc` (
  `id` int NOT NULL AUTO_INCREMENT,
  `codigo` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `actived` boolean DEFAULT true,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_s3s7txlthsdbjoi3ybjgymvpi` (`codigo`,`actived`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int NOT NULL AUTO_INCREMENT,
  `apellidos` varchar(255) NOT NULL,
  `documento` varchar(255) NOT NULL,
  `edad` tinyint NOT NULL,
  `idimagen` varchar(255) DEFAULT NULL,
  `nombres` varchar(255) NOT NULL,
  `idciudad` int NOT NULL,
  `idtipodoc` int NOT NULL,
  `actived` boolean DEFAULT true,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK6fnav7wt3x0hjsdooke795259` (`idtipodoc`,`documento`,`actived`),
  KEY `IDXrkaq8cmjr41swtifupuc9qmc1` (`edad`,`actived`),
  KEY `FK1le6orj8lrio9vg2jdayn5kqy` (`idciudad`),
  CONSTRAINT `FK1le6orj8lrio9vg2jdayn5kqy` FOREIGN KEY (`idciudad`) REFERENCES `ciudad` (`id`),
  CONSTRAINT `FK9il2l36dibniqqn5310bnlo94` FOREIGN KEY (`idtipodoc`) REFERENCES `tipodoc` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
