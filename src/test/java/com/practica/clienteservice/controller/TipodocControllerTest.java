package com.practica.clienteservice.controller;

import java.util.Optional;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.practica.clienteservice.dto.entrada.ITipodocDto;
import com.practica.clienteservice.dto.salida.OTipodocDto;
import com.practica.clienteservice.entity.TipodocEntity;
import com.practica.clienteservice.repository.TipodocRepository;
import com.practica.commonsmodule.error.Encontrado;
import com.practica.commonsmodule.error.NoEncontrado;

//pruebas de integración
//todas las pruebas son simultaneas
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(OrderAnnotation.class)
class TipodocControllerTest {
	private static final String PREFIJO_URL = "/tipodocs";
	private static final String CODIGO = "01";
	private static final String DESCRIPCION = "CEDULA";
	
	private static final String TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJ1dTRzOGRNYk"
			+ "dGLWVFa29JTmp3VEpTMDJDMlptcVdoSkYwbXlnbzdKWXhBIn0.eyJleHAiOjE2MjY5NDc1NzcsImlhdCI6MTYyNj"
			+ "kxMTU3NywianRpIjoiMDAxYzQ5NWYtYWE4MS00M2ZmLTkxZDMtYzM3NmFkMjJjMzAzIiwiaXNzIjoiaHR0cDovL2"
			+ "xvY2FsaG9zdDo4MDgwL2F1dGgvcmVhbG1zL3ByYWN0aWNhIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6ImUwYmU3MW"
			+ "MxLTg2NDEtNDJiNS1hMThjLTA1MjdlM2MyNGFlZSIsInR5cCI6IkJlYXJlciIsImF6cCI6InByYWN0aWNhMSIsIn"
			+ "Nlc3Npb25fc3RhdGUiOiI0M2QxMDU3ZS01ZDdmLTRkMDMtYjFiOC1iMTc3NWI4YjI5NDAiLCJhY3IiOiIxIiwicm"
			+ "VhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iLCJkZWZhdW"
			+ "x0LXJvbGVzLXByYWN0aWNhIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsicHJhY3RpY2ExIjp7InJvbGVzIjpbImFkbW"
			+ "luIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLC"
			+ "J2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6InByb2ZpbGUgZW1haWwiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIn"
			+ "ByZWZlcnJlZF91c2VybmFtZSI6Imx1aXMifQ.gDRBnioYBRxG3ImoJyeItjmttLaNI_xgI_8kSNHhvdf7vy2RsVI"
			+ "-vc3FUozihBkKHYRaZX88n4MEEn_ma_oUr2Tv2fNWpELPYr7syXxE5YvaWSs0qmPeg6BsAvBjoQB18CvMoWjfgR4"
			+ "_uB2rtGgxo4eSE3EJiEI4fPjfM5yG7X1QEazwUR3zeZ-M9_XoW98JvFlzFGVpm4ndk_r2WG9Mu3YyTm-tPkXN2OU"
			+ "XdqbaqXris7dO05RFduRJPJ7_qFpZtp1yft-zf4yx6SECMZxEXxfxFpqQIRKtJnrJ8q356rcoRvEkXsTTkBfC5_3"
			+ "UQQZZQd3E2YkktoX44krvqGkEMQ";

	private static Integer id;
	private static Integer idDelete;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	TipodocRepository tipodocRepository;

	@Test
	@Order(8)
	void listTest() throws Exception {
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(PREFIJO_URL + "/all")
				.header("authorization", "Bearer " + TOKEN));
		result.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	@Order(9)
	void getAllPagesTest() throws Exception {
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(PREFIJO_URL).param("pageNo", "1").param("pageSize", "1").param("sortBy", "id")
				.header("authorization", "Bearer " + TOKEN));
		result.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	@Order(6)
	void getTestWhenTipodocDtoIsPresent() throws Exception {
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(PREFIJO_URL + "/{id}", id)
				.header("authorization", "Bearer " + TOKEN));
		result.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(id)));
	}

	@Test
	@Order(7)
	void getTestWhenTipodocDtoIsNotPresent() throws Exception {
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get(PREFIJO_URL + "/{id}", idDelete)
				.header("authorization", "Bearer " + TOKEN));
		result.andExpect(MockMvcResultMatchers.status().isNotFound())
			.andExpect(resultLambda -> Assertions.assertTrue(resultLambda.getResolvedException() instanceof NoEncontrado));
	}

	@Test
	@Order(2)
	void addTestWhenIdIsPresent() throws Exception {
		ITipodocDto iTipodocDto = ITipodocDto.builder().id(1).codigo(CODIGO).descripcion(DESCRIPCION).build();

		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(PREFIJO_URL)
				.content(objectMapper.writeValueAsString(iTipodocDto))
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", "Bearer " + TOKEN));
		result.andExpect(MockMvcResultMatchers.status().isFound())
			.andExpect(resultLambda -> Assertions.assertTrue(resultLambda.getResolvedException() instanceof Encontrado));
	}

	@Test
	@Order(1)
	void addTestWhenTipodocDtoIsNotPresent() throws Exception {
		Optional<TipodocEntity> tipodoc = tipodocRepository.findByCodigo(CODIGO);
		if (tipodoc.isPresent()) {
			tipodocRepository.delete(tipodoc.get());
		}
		
		ITipodocDto iTipodocDto = ITipodocDto.builder().id(0).codigo(CODIGO).descripcion(DESCRIPCION).build();

		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(PREFIJO_URL)
				.content(objectMapper.writeValueAsString(iTipodocDto))
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", "Bearer " + TOKEN));
		result.andExpect(MockMvcResultMatchers.status().isCreated())
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.notNullValue()))
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.not(0)))
			.andExpect(MockMvcResultMatchers.jsonPath("$.codigo", CoreMatchers.is(CODIGO)));
		
		MvcResult mvcResult = result.andReturn();
		String contentAsString = mvcResult.getResponse().getContentAsString();
		OTipodocDto TipodocDtoResponse = objectMapper.readValue(contentAsString, OTipodocDto.class);
		
		id = TipodocDtoResponse.getId();
		TipodocEntity tipodocEntity = tipodocRepository.save(TipodocEntity.builder().id(0).codigo(CODIGO + "x").descripcion(DESCRIPCION).build());
		idDelete = tipodocEntity.getId();
		tipodocRepository.delete(tipodocEntity);
	}

	@Test
	@Order(3)
	void addTestWhenTipodocDtoIsPresent() throws Exception {
		ITipodocDto iTipodocDto = ITipodocDto.builder().id(0).codigo(CODIGO).descripcion(DESCRIPCION).build();

		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post(PREFIJO_URL)
				.content(objectMapper.writeValueAsString(iTipodocDto))
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", "Bearer " + TOKEN));
		result.andExpect(MockMvcResultMatchers.status().isFound())
			.andExpect(resultLambda -> Assertions.assertTrue(resultLambda.getResolvedException() instanceof Encontrado));
	}

	@Test
	@Order(4)
	void updateTestWhenTipodocDtoIsPresent() throws Exception {
		ITipodocDto iTipodocDto = ITipodocDto.builder().id(id).codigo(CODIGO).descripcion(DESCRIPCION).build();

		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.put(PREFIJO_URL)
				.content(objectMapper.writeValueAsString(iTipodocDto))
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", "Bearer " + TOKEN));
		result.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(id)));
	}

	@Test
	@Order(5)
	void updateTestWhenTipodocDtoIsNotPresent() throws Exception {
		ITipodocDto iTipodocDto = ITipodocDto.builder().id(idDelete).codigo(CODIGO).descripcion(DESCRIPCION).build();

		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.put(PREFIJO_URL)
				.content(objectMapper.writeValueAsString(iTipodocDto))
				.contentType(MediaType.APPLICATION_JSON)
				.header("authorization", "Bearer " + TOKEN));
		result.andExpect(MockMvcResultMatchers.status().isNotFound())
			.andExpect(resultLambda -> Assertions.assertTrue(resultLambda.getResolvedException() instanceof NoEncontrado));
	}
	
	@Test
	@Order(10)
	void deleteTestWhenTipodocDtoIsPresent() throws Exception {
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.delete(PREFIJO_URL + "/{id}", id)
				.header("authorization", "Bearer " + TOKEN));
		result.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(id)));
	}
	
	@Test
	@Order(11)
	void deleteTestWhenTipodocDtoIsNotPresent() throws Exception {	
		final ResultActions result = mockMvc.perform(MockMvcRequestBuilders.delete(PREFIJO_URL + "/{id}", idDelete)
				.header("authorization", "Bearer " + TOKEN));
		result.andExpect(MockMvcResultMatchers.status().isNotFound())
			.andExpect(resultLambda -> Assertions.assertTrue(resultLambda.getResolvedException() instanceof NoEncontrado));  //expresión lambda es una función sin nombre
		//Assertions.assertThrows(NoEncontrado.class, () -> tipodocController.delete(idtemp));  //expresión lambda es una función sin nombre
	}

}
