package com.practica.clienteservice.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.practica.clienteservice.dto.entrada.ITipodocDto;
import com.practica.clienteservice.dto.salida.OTipodocDto;
import com.practica.clienteservice.entity.TipodocEntity;
import com.practica.clienteservice.repository.TipodocRepository;
import com.practica.clienteservice.service.impl.TipodocServiceImpl;
import com.practica.clienteservice.util.TipodocConverter;
import com.practica.commonsmodule.error.Encontrado;
import com.practica.commonsmodule.error.NoEncontrado;

//pruebas unitarias
//todas las pruebas son simultaneas
@ExtendWith(MockitoExtension.class)
class TipodocServiceImplTest {

	@InjectMocks
	TipodocServiceImpl tipodocServiceImpl;
	
	@Mock
	TipodocRepository tipodocRepository;
	
	@Mock
	TipodocEntity tipodocEntity;
	
	@Mock
	OTipodocDto oTipodocDto;
	
	@Mock
	ITipodocDto iTipodocDto;
	
	private static final int ID = 1;
	private static MockedStatic<TipodocConverter> tipodocConverter;
	
	@BeforeAll
	public static void ini() {
		tipodocConverter = Mockito.mockStatic(TipodocConverter.class);
	}
	
	@AfterAll
	public static void fin() {
		tipodocConverter.close();
	}
	
	@Test
	void findAllTest() {				
		List<TipodocEntity> tipodocesDocument = new ArrayList<>();
		tipodocesDocument.add(tipodocEntity);
		Mockito.when(tipodocRepository.findAll()).thenReturn(tipodocesDocument);

		Assertions.assertEquals(tipodocesDocument.size(), tipodocServiceImpl.findAll().size());
	}
	
	@Test
	void getAllPagesTest() {
		List<TipodocEntity> tipodocesDocument = new ArrayList<>();
		tipodocesDocument.add(tipodocEntity);
		Pageable paging = PageRequest.of(1, 1, Sort.by("codigo"));
		
		Page<TipodocEntity> tipodocesPageDocument = new PageImpl<>(tipodocesDocument, PageRequest.of(1, 1), 400L);
		Mockito.when(tipodocRepository.findAll(paging)).thenReturn(tipodocesPageDocument);

		Assertions.assertEquals(tipodocesDocument.size(), tipodocServiceImpl.getAllPages(1, 1, "codigo").getSize());
	}
	
	@Test
	void findOneTestWhenTipodocDtoIsNotPresent() {		
		Mockito.when(tipodocRepository.findById(ID)).thenReturn(Optional.ofNullable(null));

		Assertions.assertThrows(NoEncontrado.class, () -> tipodocServiceImpl.findOne(ID));
	}
	
	@Test
	void findOneTestWhenTipodocDtoIsPresent() {		
		Mockito.when(tipodocRepository.findById(ID)).thenReturn(Optional.of(tipodocEntity));
		tipodocConverter.when(() -> TipodocConverter.convertTipodocEntityToOTipodocDto(tipodocEntity)).thenReturn(oTipodocDto);
		Mockito.when(oTipodocDto.getId()).thenReturn(ID);

		Assertions.assertEquals(ID, tipodocServiceImpl.findOne(ID).getId());
	}

	@Test
	void saveTestWhenIdIsPresent() {
		Mockito.when(iTipodocDto.getId()).thenReturn(ID);
		
		Assertions.assertThrows(Encontrado.class, () -> tipodocServiceImpl.save(iTipodocDto));
	}
	
	@Test
	void saveTestWhenTipodocDtoIsNotPresent() {
		Mockito.when(iTipodocDto.getId()).thenReturn(0);
		Mockito.when(oTipodocDto.getId()).thenReturn(0);
		tipodocConverter.when(() -> TipodocConverter.convertITipodocDtoToTipodocEntity(iTipodocDto)).thenReturn(tipodocEntity);

		Mockito.when(tipodocRepository.save(tipodocEntity)).thenReturn(tipodocEntity);
		tipodocConverter.when(() -> TipodocConverter.convertTipodocEntityToOTipodocDto(tipodocEntity)).thenReturn(oTipodocDto);
		
		Assertions.assertEquals(TipodocConverter.convertTipodocEntityToOTipodocDto(tipodocEntity).getId(), tipodocServiceImpl.save(iTipodocDto).getId());
	}
	
	@Test
	void saveTestWhenTipodocDtoIsPresent() {		
		Mockito.when(iTipodocDto.getId()).thenReturn(ID);
		
		Assertions.assertThrows(Encontrado.class, () -> tipodocServiceImpl.save(iTipodocDto));
	}
	
	@Test
	void updateTestWhenTipodocDtoIsNotPresent() {
		Mockito.when(iTipodocDto.getId()).thenReturn(ID);

		Mockito.when(tipodocRepository.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		Assertions.assertThrows(NoEncontrado.class, () -> tipodocServiceImpl.update(iTipodocDto));
	}
	
	@Test
	void updateTestWhenTipodocDtoIsPresent() {
		Mockito.when(iTipodocDto.getId()).thenReturn(ID);
		Mockito.when(oTipodocDto.getId()).thenReturn(ID);

		Mockito.when(tipodocRepository.findById(ID)).thenReturn(Optional.of(tipodocEntity));
		
		Mockito.when(tipodocRepository.findByIdNotAndCodigo(ID, iTipodocDto.getCodigo())).thenReturn(Optional.ofNullable(null));
		
		tipodocConverter.when(() -> TipodocConverter.convertITipodocDtoToTipodocEntity(iTipodocDto)).thenReturn(tipodocEntity);
		tipodocConverter.when(() -> TipodocConverter.convertTipodocEntityToOTipodocDto(tipodocEntity)).thenReturn(oTipodocDto);
		Mockito.when(tipodocRepository.save(tipodocEntity)).thenReturn(tipodocEntity);
		
		Assertions.assertEquals(ID, tipodocServiceImpl.update(iTipodocDto).getId());
	}
	
	@Test
	void updateTestWhenCodigoIsPresent() {
		Mockito.when(iTipodocDto.getId()).thenReturn(ID);

		Mockito.when(tipodocRepository.findById(ID)).thenReturn(Optional.of(tipodocEntity));

		Mockito.when(tipodocRepository.findByIdNotAndCodigo(ID, iTipodocDto.getCodigo())).thenReturn(Optional.ofNullable(tipodocEntity));
		
		Assertions.assertThrows(Encontrado.class, () -> tipodocServiceImpl.update(iTipodocDto));
	}
	
	@Test
	void deleteTestWhenTipodocDtoIsNotPresent() {
		Mockito.when(tipodocRepository.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		Assertions.assertThrows(NoEncontrado.class, () -> tipodocServiceImpl.deleteOne(ID));
	}
	
	@Test
	void deleteTestWhenTipodocDtoIsPresent() {
		Mockito.when(tipodocRepository.findById(ID)).thenReturn(Optional.of(tipodocEntity));
		tipodocConverter.when(() -> TipodocConverter.convertTipodocEntityToOTipodocDto(tipodocEntity)).thenReturn(oTipodocDto);
		Mockito.when(oTipodocDto.getId()).thenReturn(ID);
		
		Assertions.assertEquals(ID, tipodocServiceImpl.deleteOne(ID).getId());
	}

}
