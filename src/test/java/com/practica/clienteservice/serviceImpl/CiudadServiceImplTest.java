package com.practica.clienteservice.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.practica.clienteservice.dto.entrada.ICiudadDto;
import com.practica.clienteservice.dto.salida.OCiudadDto;
import com.practica.clienteservice.entity.CiudadEntity;
import com.practica.clienteservice.repository.CiudadRepository;
import com.practica.clienteservice.service.impl.CiudadServiceImpl;
import com.practica.clienteservice.util.CiudadConverter;
import com.practica.commonsmodule.error.Encontrado;
import com.practica.commonsmodule.error.NoEncontrado;

//pruebas unitarias
//todas las pruebas son simultaneas
@ExtendWith(MockitoExtension.class)
class CiudadServiceImplTest {

	@InjectMocks
	CiudadServiceImpl ciudadServiceImpl;
	
	@Mock
	CiudadRepository ciudadRepository;
	
	@Mock
	CiudadEntity ciudadEntity;
	
	@Mock
	OCiudadDto oCiudadDto;
	
	@Mock
	ICiudadDto iCiudadDto;
	
	private static final int ID = 1;	
	private static MockedStatic<CiudadConverter> ciudadConverter;
	
	@BeforeAll
	public static void ini() {
		ciudadConverter = Mockito.mockStatic(CiudadConverter.class);
	}
	
	@AfterAll
	public static void fin() {
		ciudadConverter.close();
	}
	
	@Test
	void findAllTest() {				
		List<CiudadEntity> ciudadesDocument = new ArrayList<>();
		ciudadesDocument.add(ciudadEntity);
		Mockito.when(ciudadRepository.findAll()).thenReturn(ciudadesDocument);

		Assertions.assertEquals(ciudadesDocument.size(), ciudadServiceImpl.findAll().size());
	}
	
	@Test
	void getAllPagesTest() {
		List<CiudadEntity> ciudadesDocument = new ArrayList<>();
		ciudadesDocument.add(ciudadEntity);
		Pageable paging = PageRequest.of(1, 1, Sort.by("codigo"));
		
		Page<CiudadEntity> ciudadesPageDocument = new PageImpl<>(ciudadesDocument, PageRequest.of(1, 1), 400L);
		Mockito.when(ciudadRepository.findAll(paging)).thenReturn(ciudadesPageDocument);

		Assertions.assertEquals(ciudadesDocument.size(), ciudadServiceImpl.getAllPages(1, 1, "codigo").getSize());
	}
	
	@Test
	void findOneTestWhenCiudadDtoIsNotPresent() {		
		Mockito.when(ciudadRepository.findById(ID)).thenReturn(Optional.ofNullable(null));

		Assertions.assertThrows(NoEncontrado.class, () -> ciudadServiceImpl.findOne(ID));
	}
	
	@Test
	void findOneTestWhenCiudadDtoIsPresent() {		
		Mockito.when(ciudadRepository.findById(ID)).thenReturn(Optional.of(ciudadEntity));
		ciudadConverter.when(() -> CiudadConverter.convertCiudadEntityToOCiudadDto(ciudadEntity)).thenReturn(oCiudadDto);
		Mockito.when(oCiudadDto.getId()).thenReturn(ID);

		Assertions.assertEquals(ID, ciudadServiceImpl.findOne(ID).getId());
	}

	@Test
	void saveTestWhenIdIsPresent() {
		Mockito.when(iCiudadDto.getId()).thenReturn(ID);
		
		Assertions.assertThrows(Encontrado.class, () -> ciudadServiceImpl.save(iCiudadDto));
	}
	
	@Test
	void saveTestWhenCiudadDtoIsNotPresent() {
		Mockito.when(iCiudadDto.getId()).thenReturn(0);
		Mockito.when(oCiudadDto.getId()).thenReturn(0);
		ciudadConverter.when(() -> CiudadConverter.convertICiudadDtoToCiudadEntity(iCiudadDto)).thenReturn(ciudadEntity);

		Mockito.when(ciudadRepository.save(ciudadEntity)).thenReturn(ciudadEntity);
		ciudadConverter.when(() -> CiudadConverter.convertCiudadEntityToOCiudadDto(ciudadEntity)).thenReturn(oCiudadDto);
		
		Assertions.assertEquals(CiudadConverter.convertCiudadEntityToOCiudadDto(ciudadEntity).getId(), ciudadServiceImpl.save(iCiudadDto).getId());
	}
	
	@Test
	void saveTestWhenCiudadDtoIsPresent() {		
		Mockito.when(iCiudadDto.getId()).thenReturn(ID);
		
		Assertions.assertThrows(Encontrado.class, () -> ciudadServiceImpl.save(iCiudadDto));
	}
	
	@Test
	void updateTestWhenCiudadDtoIsNotPresent() {
		Mockito.when(iCiudadDto.getId()).thenReturn(ID);

		Mockito.when(ciudadRepository.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		Assertions.assertThrows(NoEncontrado.class, () -> ciudadServiceImpl.update(iCiudadDto));
	}
	
	@Test
	void updateTestWhenCiudadDtoIsPresent() {
		Mockito.when(iCiudadDto.getId()).thenReturn(ID);
		Mockito.when(oCiudadDto.getId()).thenReturn(ID);

		Mockito.when(ciudadRepository.findById(ID)).thenReturn(Optional.of(ciudadEntity));

		Mockito.when(ciudadRepository.findByIdNotAndCodigo(ID, iCiudadDto.getCodigo())).thenReturn(Optional.ofNullable(null));
		
		ciudadConverter.when(() -> CiudadConverter.convertICiudadDtoToCiudadEntity(iCiudadDto)).thenReturn(ciudadEntity);
		ciudadConverter.when(() -> CiudadConverter.convertCiudadEntityToOCiudadDto(ciudadEntity)).thenReturn(oCiudadDto);
		Mockito.when(ciudadRepository.save(ciudadEntity)).thenReturn(ciudadEntity);
		
		Assertions.assertEquals(ID, ciudadServiceImpl.update(iCiudadDto).getId());
	}
	
	@Test
	void updateTestWhenCodigoIsPresent() {
		Mockito.when(iCiudadDto.getId()).thenReturn(ID);

		Mockito.when(ciudadRepository.findById(ID)).thenReturn(Optional.of(ciudadEntity));

		Mockito.when(ciudadRepository.findByIdNotAndCodigo(ID, iCiudadDto.getCodigo())).thenReturn(Optional.ofNullable(ciudadEntity));
		
		Assertions.assertThrows(Encontrado.class, () -> ciudadServiceImpl.update(iCiudadDto));
	}
	
	@Test
	void deleteTestWhenCiudadDtoIsNotPresent() {
		Mockito.when(ciudadRepository.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		Assertions.assertThrows(NoEncontrado.class, () -> ciudadServiceImpl.deleteOne(ID));
	}
	
	@Test
	void deleteTestWhenCiudadDtoIsPresent() {
		Mockito.when(ciudadRepository.findById(ID)).thenReturn(Optional.of(ciudadEntity));
		ciudadConverter.when(() -> CiudadConverter.convertCiudadEntityToOCiudadDto(ciudadEntity)).thenReturn(oCiudadDto);
		Mockito.when(oCiudadDto.getId()).thenReturn(ID);
		
		Assertions.assertEquals(ID, ciudadServiceImpl.deleteOne(ID).getId());
	}

}
