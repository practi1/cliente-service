package com.practica.clienteservice.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.practica.clienteservice.dto.entrada.ICiudadDto;
import com.practica.clienteservice.dto.entrada.IClienteDto;
import com.practica.clienteservice.dto.entrada.ITipodocDto;
import com.practica.clienteservice.dto.salida.OClienteDto;
import com.practica.clienteservice.entity.CiudadEntity;
import com.practica.clienteservice.entity.ClienteEntity;
import com.practica.clienteservice.entity.TipodocEntity;
import com.practica.clienteservice.feign.ImagenFeign;
import com.practica.clienteservice.repository.CiudadRepository;
import com.practica.clienteservice.repository.ClienteRepository;
import com.practica.clienteservice.repository.TipodocRepository;
import com.practica.clienteservice.service.impl.ClienteServiceImpl;
import com.practica.clienteservice.util.CiudadConverter;
import com.practica.clienteservice.util.ClienteConverter;
import com.practica.clienteservice.util.TipodocConverter;
import com.practica.commonsmodule.error.Encontrado;
import com.practica.commonsmodule.error.NoEncontrado;

//pruebas unitarias
//todas las pruebas son simultaneas
@ExtendWith(MockitoExtension.class)
class ClienteServiceImplTest {

	@InjectMocks
	ClienteServiceImpl clienteServiceImpl;
	
	@Mock
	ClienteRepository clienteRepository;
	
	@Mock
	ImagenFeign imagenFeign;
	
	@Mock
	private CiudadRepository ciudadRepository;
	
	@Mock
	private TipodocRepository tipodocRepository;
	
	@Mock
	ClienteEntity clienteEntity;
	
	@Mock
	CiudadEntity ciudadEntity;
	
	@Mock
	TipodocEntity tipodocEntity;
	
	@Mock
	IClienteDto iClienteDto;
	
	@Mock
	OClienteDto oClienteDto;
	
	@Mock
	ICiudadDto iCiudadDto;
	
	@Mock
	ITipodocDto iTipodocDto;
	
	private static final int ID = 1;	
	private static MockedStatic<ClienteConverter> clienteConverter;
	private static MockedStatic<TipodocConverter> tipodocConverter;
	private static MockedStatic<CiudadConverter> ciudadConverter;
	
	@BeforeAll
	public static void ini() {
		clienteConverter = Mockito.mockStatic(ClienteConverter.class);
		tipodocConverter = Mockito.mockStatic(TipodocConverter.class);
		ciudadConverter = Mockito.mockStatic(CiudadConverter.class);
	}
	
	@AfterAll
	public static void fin() {
		clienteConverter.close();
		tipodocConverter.close();
		ciudadConverter.close();
	}
	
	@Test
	void findAllTest() {				
		List<ClienteEntity> clientesDocument = new ArrayList<>();
		clientesDocument.add(clienteEntity);
		Mockito.when(clienteRepository.findAll()).thenReturn(clientesDocument);

		Assertions.assertEquals(clientesDocument.size(), clienteServiceImpl.findAll().size());
	}
	
	@Test
	void getAllPagesTest() {
		List<ClienteEntity> clientesDocument = new ArrayList<>();
		clientesDocument.add(clienteEntity);
		Pageable paging = PageRequest.of(1, 1, Sort.by("documento"));
		
		Page<ClienteEntity> clientesPageDocument = new PageImpl<>(clientesDocument, PageRequest.of(1, 1), 400L);
		Mockito.when(clienteRepository.findAll(paging)).thenReturn(clientesPageDocument);

		Assertions.assertEquals(clientesDocument.size(), clienteServiceImpl.getAllPages(1, 1, "documento").getSize());
	}
	
	@Test
	void findOneTestWhenClienteDtoIsNotPresent() {		
		Mockito.when(clienteRepository.findById(ID)).thenReturn(Optional.ofNullable(null));

		Assertions.assertThrows(NoEncontrado.class, () -> clienteServiceImpl.findOne(ID));
	}
	
	@Test
	void findOneTestWhenClienteDtoIsPresent() {
		Mockito.when(clienteRepository.findById(ID)).thenReturn(Optional.of(clienteEntity));
		clienteConverter.when(() -> ClienteConverter.convertClienteEntityToOClienteDto(clienteEntity)).thenReturn(oClienteDto);
		Mockito.when(oClienteDto.getId()).thenReturn(ID);

		Assertions.assertEquals(ID, clienteServiceImpl.findOne(ID).getId());
	}

	@Test
	void saveTestWhenIdIsPresent() {
		Mockito.when(iClienteDto.getId()).thenReturn(ID);
		
		Assertions.assertThrows(Encontrado.class, () -> clienteServiceImpl.save(iClienteDto));
	}
	
	@Test
	void saveTestWhenClienteDtoIsNotPresent() {
		Mockito.when(iClienteDto.getId()).thenReturn(0);
		Mockito.when(oClienteDto.getId()).thenReturn(0);
		Mockito.when(iClienteDto.getIdTipodoc()).thenReturn(1);
		Mockito.when(tipodocRepository.findById(1)).thenReturn(Optional.of(tipodocEntity));
		tipodocConverter.when(() -> TipodocConverter.convertTipodocEntityToITipodocDto(tipodocEntity)).thenReturn(iTipodocDto);
		Mockito.when(iClienteDto.getIdCiudad()).thenReturn(1);
		Mockito.when(ciudadRepository.findById(1)).thenReturn(Optional.of(ciudadEntity));
		ciudadConverter.when(() -> CiudadConverter.convertCiudadEntityToICiudadDto(ciudadEntity)).thenReturn(iCiudadDto);
		Mockito.when(clienteRepository.findByTipodocIdAndDocumento(iClienteDto.getIdTipodoc(), iClienteDto.getDocumento())).thenReturn(Optional.ofNullable(null));
		clienteConverter.when(() -> ClienteConverter.convertIClienteDtoToClienteEntity(iClienteDto)).thenReturn(clienteEntity);

		Mockito.when(clienteRepository.save(clienteEntity)).thenReturn(clienteEntity);
		clienteConverter.when(() -> ClienteConverter.convertClienteEntityToOClienteDto(clienteEntity)).thenReturn(oClienteDto);
		
		Assertions.assertEquals(ClienteConverter.convertClienteEntityToOClienteDto(clienteEntity).getId(), clienteServiceImpl.save(iClienteDto).getId());
	}
	
	@Test
	void saveTestWhenClienteDtoIsPresent() {		
		Mockito.when(iClienteDto.getId()).thenReturn(ID);
		
		Assertions.assertThrows(Encontrado.class, () -> clienteServiceImpl.save(iClienteDto));
	}
	
	@Test
	void updateTestWhenClienteDtoIsNotPresent() {
		Mockito.when(iClienteDto.getId()).thenReturn(ID);

		Mockito.when(clienteRepository.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		Assertions.assertThrows(NoEncontrado.class, () -> clienteServiceImpl.update(iClienteDto));
	}
	
	@Test
	void updateTestWhenClienteDtoIsPresent() {
		Mockito.when(iClienteDto.getId()).thenReturn(ID);
		Mockito.when(oClienteDto.getId()).thenReturn(ID);
		Mockito.when(iClienteDto.getIdTipodoc()).thenReturn(1);
		Mockito.when(tipodocRepository.findById(1)).thenReturn(Optional.of(tipodocEntity));
		tipodocConverter.when(() -> TipodocConverter.convertTipodocEntityToITipodocDto(tipodocEntity)).thenReturn(iTipodocDto);
		Mockito.when(iClienteDto.getIdCiudad()).thenReturn(1);
		Mockito.when(ciudadRepository.findById(1)).thenReturn(Optional.of(ciudadEntity));
		ciudadConverter.when(() -> CiudadConverter.convertCiudadEntityToICiudadDto(ciudadEntity)).thenReturn(iCiudadDto);

		Mockito.when(clienteRepository.findById(ID)).thenReturn(Optional.of(clienteEntity));
		
		Mockito.when(clienteRepository.findByIdNotAndTipodocIdAndDocumento(ID, iClienteDto.getIdTipodoc(), iClienteDto.getDocumento())).thenReturn(Optional.ofNullable(null));
		
		clienteConverter.when(() -> ClienteConverter.convertIClienteDtoToClienteEntity(iClienteDto)).thenReturn(clienteEntity);
		clienteConverter.when(() -> ClienteConverter.convertClienteEntityToOClienteDto(clienteEntity)).thenReturn(oClienteDto);
		Mockito.when(clienteRepository.save(clienteEntity)).thenReturn(clienteEntity);
		
		Assertions.assertEquals(ID, clienteServiceImpl.update(iClienteDto).getId());
	}
	
	@Test
	void updateTestWhenCodigoIsPresent() {
		Mockito.when(iClienteDto.getId()).thenReturn(ID);

		Mockito.when(clienteRepository.findById(ID)).thenReturn(Optional.of(clienteEntity));

		Mockito.when(clienteRepository.findByIdNotAndTipodocIdAndDocumento(ID, iClienteDto.getIdTipodoc(), iClienteDto.getDocumento())).thenReturn(Optional.ofNullable(clienteEntity));
		
		Assertions.assertThrows(Encontrado.class, () -> clienteServiceImpl.update(iClienteDto));
	}
	
	@Test
	void deleteTestWhenClienteDtoIsNotPresent() {
		Mockito.when(clienteRepository.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		Assertions.assertThrows(NoEncontrado.class, () -> clienteServiceImpl.deleteOne(ID));
	}
	
	@Test
	void deleteTestWhenClienteDtoIsPresent() {
		Mockito.when(clienteRepository.findById(ID)).thenReturn(Optional.of(clienteEntity));
		clienteConverter.when(() -> ClienteConverter.convertClienteEntityToOClienteDto(clienteEntity)).thenReturn(oClienteDto);
		Mockito.when(oClienteDto.getId()).thenReturn(ID);
		
		Assertions.assertEquals(ID, clienteServiceImpl.deleteOne(ID).getId());
	}

}
