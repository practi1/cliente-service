package com.practica.clienteservice;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.practica.clienteservice.controller.CiudadController;
import com.practica.clienteservice.controller.ClienteController;
import com.practica.clienteservice.controller.TipodocController;

@SpringBootTest
class ClienteServiceApplicationTests {
	
	@Autowired
	 private CiudadController ciudadController;
	
	@Autowired
	 private ClienteController clienteController;
	
	@Autowired
	 private TipodocController tipodocController;

	@Test
	void contextLoads() {
		Assertions.assertNotNull(ciudadController);
		Assertions.assertNotNull(clienteController);
		Assertions.assertNotNull(tipodocController);
	}

}
